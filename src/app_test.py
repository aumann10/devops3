from main import app
import unittest

class flaskTestCase(unittest.TestCase):

    def test_index(self):
        tester = app.test_client(self)
        response = tester.get('/', content_type='html_text')
        self.assertEqual(response.status_code, 200)

    def test_sum1(self):
        tester = app.test_client(self)
        response = tester.get('/10/+/5', content_type='html_text')
        self.assertTrue(response.status_code, 200)
        self.assertTrue(b'15' in response.data)

    def test_sum2(self):
        tester = app.test_client(self)
        response = tester.get('/10/+/5', content_type='html_text')
        self.assertTrue(response.status_code, 200)
        self.assertFalse(b'13' in response.data)

    def test_minus1(self):
            tester = app.test_client(self)
            response = tester.get('/10/-/5', content_type='html_text')
            self.assertTrue(response.status_code, 200)
            self.assertTrue(b'5' in response.data)

    def test_minus2(self):
            tester = app.test_client(self)
            response = tester.get('/10/-/5', content_type='html_text')
            self.assertTrue(response.status_code, 200)
            self.assertFalse(b'15' in response.data)

    def test_mult1(self):
        tester = app.test_client(self)
        response = tester.get('/10/*/5', content_type='html_text')
        self.assertTrue(response.status_code, 200)
        self.assertTrue(b'50' in response.data)

    def test_mult2(self):
        tester = app.test_client(self)
        response = tester.get('/10/*/5', content_type='html_text')
        self.assertTrue(response.status_code, 200)
        self.assertFalse(b'24' in response.data)

    def test_mult3(self):
        tester = app.test_client(self)
        response = tester.get('/10/*/0', content_type='html_text')
        self.assertTrue(response.status_code, 200)
        self.assertTrue(b'0' in response.data)

    def test_mult4(self):
        tester = app.test_client(self)
        response = tester.get('/10/*/0', content_type='html_text')
        self.assertTrue(response.status_code, 200)
        self.assertFalse(b'13' in response.data)

    def test_div1(self):
        tester = app.test_client(self)
        response = tester.get('/10/div/5', content_type='html_text')
        self.assertTrue(response.status_code, 200)
        self.assertTrue(b'2' in response.data)

    def test_div2(self):
        tester = app.test_client(self)
        response = tester.get('/10/div/12', content_type='html_text')
        self.assertTrue(response.status_code, 200)
        self.assertTrue(b'0' in response.data)

    def test_div3(self):
        tester = app.test_client(self)
        response = tester.get('/10/div/3', content_type='html_text')
        self.assertTrue(response.status_code, 200)
        self.assertTrue(b'3' in response.data)

    
if __name__ == '__main__':
    unittest.main()
