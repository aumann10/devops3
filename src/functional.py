from flask import Flask

app = Flask(__name__)

@app.route('/<int:firstOp>/<oper>/<int:secondOp>', methods=['GET'])
def operation(firstOp, secondOp, oper):
    if oper == '+':
        return str(firstOp + secondOp)
    if oper == '-':
        return str(firstOp - secondOp)
    if oper == '*':
        return str(firstOp * secondOp)
    if oper == 'div':
        return str(firstOp / secondOp)
    else:
        return f'Unexpected operator {oper}'


if __name__ == '__main__':
    app.run(debug=True)