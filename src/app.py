import tornado.web
import functional
app = tornado.web.Application([(r"/Calc", tornado.web.RequestHandler)])
if __name__ == "__main__":
    app.listen(8881)
    print("Open browser")
    tornado.ioloop.IOLoop.current().start()
